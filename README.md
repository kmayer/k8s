## Установка виртуалки
В качестве виртуальной машины используем `virtualbox` из-за дружелюбного интерфейса.
```bash
$ sudo apt-get update && \
sudo apt-get install virtualbox -y
```

## Установка kubectl
Утилита `kubectl` представляет из себя cli-инструмент для работы с api `kubernetes`.
```bash
$ sudo apt-get update && sudo apt-get install -y apt-transport-https
$ curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
$ echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list
$ sudo apt-get update
$ sudo apt-get install -y kubectl
```

## Установка minikube
Инструмент для разработки. Позволяет запускать настроенный кластер `kubernetes` на виртуальной машине.
```bash
$ curl -Lo minikube https://storage.googleapis.com/minikube/releases/v0.31.0/minikube-linux-amd64 && \
chmod +x minikube && \
sudo cp minikube /usr/local/bin/ && \
rm minikube
```

## Добавляем ресурсов
По  умолчанию выделяется 2048Mb и 2 ядра.
```bash
$ minikube config set cpus 4
$ minikube config set memory 4096
$ minikube config set vm-driver virtualbox
```

## Запуск minikube
```bash
$ minikube start
```

## Инициализация ingress в minikube
```bash
$ minikube addons enable ingress
```

## Проверка работы
##### Запуск нашего приложения:
```bash
$ kubectl apply -f deployment.yaml
```
##### В цикле кидаем запросы к нашему приложению.
```bash
$ ip=$(minikube ip) && while true; do curl $ip; done
```
##### В ответе получаем привет от рандомного пода - работает балансировщик.
```bash
$ ip=$(minikube ip) && while true; do curl $ip; done
Hello from nginx-deployment-5cccfdb467-mpbw5!
Hello from nginx-deployment-5cccfdb467-q6p2t!
Hello from nginx-deployment-5cccfdb467-v9krd!
Hello from nginx-deployment-5cccfdb467-bpx6v!
Hello from nginx-deployment-5cccfdb467-vnnqj!
Hello from nginx-deployment-5cccfdb467-7hz62!
^C
```

## Работа с файлами
При работе с драйвером `virtualbox` на `minikube` по умолчанию монитруется ваша локальная директория `/home` в `/hosthome` на виртуалке.
Исходя из этого мы можем получить доступ к нужным нам файлам из пода.
```yaml
spec:
  containers:
  - name: nginx
    image: nginx:1.15.4
    ports:
    - containerPort: 80
    --- skip ---
    volumeMounts:
    - name: test-volume
      mountPath: /path/to/mount
  volumes:
  - name: test-volume
    hostPath:
      path: /hosthome/username/path/to/project
```

## Ресурсы
- [Kubernetes](https://kubernetes.io/)
- [Virtualbox](https://www.virtualbox.org/)
- [Видео со слёрма](https://www.youtube.com/playlist?list=PL8D2P0ruohOCzhzidhhPc8A_99tZvTmyG)
