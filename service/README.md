### Демо-сервис на nodejs с использованием swagger

Ресурсы:
- `/test` - возвращает hostname
- `/docs` - swagger-документация к сервису
- `/service/ui` - статистика по сервису

Деплой:
```bash
$ kubectl apply -f deploy.yaml
```

Дать нагрузку:
```bash
$ ip=$(minikube ip) && while true; do curl $ip/test; done
```