'use strict';

const os = require('os');

module.exports.testRoute = (req, res, next) => {
  res.send(os.hostname() + '\n');
};