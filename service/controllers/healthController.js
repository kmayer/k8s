'use strict';

module.exports.healthRoute = (req, res, next) => {
  res.json({status: 'UP'});
};